# Use uma imagem PHP específica
FROM php:7.4.0-apache
# Defina o diretório de trabalho
WORKDIR /var/www/html

# Copie todos os arquivos para o diretório de trabalho
COPY . /var/www/html
# Instale as dependências do sistema
RUN apt-get update && apt-get install -y \
    libzip-dev \
    libpng-dev \
    wget \
    unzip \
    gnupg2 \
    ca-certificates

RUN docker-php-ext-install zip
# Instale a extensão mysqli
RUN docker-php-ext-install mysqli

### MYSQL

RUN apt update && apt install -y mariadb-server mariadb-client && apt install -y  net-tools && apt install -y vim 


# Instale as extensões GD
RUN apt-get update \
    && apt-get install -y libpng-dev \
    && docker-php-ext-install gd


# Instale as dependências do Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && rm composer-setup.php


RUN composer self-update
RUN composer update --no-dev
#RUN rm -f composer.lock
RUN composer self-update --2

# Execute o Composer install
RUN composer install --no-dev

# Exponha a porta do Apache
EXPOSE 80

# Exibir as permissões antes da alteração
RUN ls -l /usr/local/bin/docker-php-entrypoint

# Conceder permissões de execução ao docker-php-entrypoint
RUN chmod +x /usr/local/bin/docker-php-entrypoint

# Exibir as permissões após a alteração (opcional)
RUN ls -l /usr/local/bin/docker-php-entrypoint
COPY ./test.sh  /usr/local/bin
RUN chmod +x   /usr/local/bin/test.sh

ENTRYPOINT ["bash", "/usr/local/bin/test.sh"]

