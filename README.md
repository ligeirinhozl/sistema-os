# Sistema Os utilzando Docker
### Requerimentos
* PHP >= 8.1
* MySQL
* Composer

# Container 

## Execução 

## Build e Criar o Container

```
docker image  build  -t update/sistema:4.0   .

```

```
docker run --name sistema   -p 80:80 -d update/sistema:4.0 
```

### Acessar o Container


```
docker exec -it sistema bash
```

## To install the MariaDB database server and client, run the following command.
## Next, confirm that the MariaDB database service is running and is enabled to automatically start when your system is restarted.
```
apt install mariadb-server mariadb-client
/etc/init.d/mysql restart 
```

## On production servers, you need to enable some basic security measures for the MariaDB database installation, by running the mysql_secure_installation script which ships with the MariaDB package.

```
mysql_secure_installation
```
### So you need to create one as shown in the following screenshot.

*Enter current password for root (enter for none): Enter*
*Set a root password? [Y/n] y*
*Remove anonymous users? [Y/n] y*
*Disallow root login remotely? [Y/n] y*
*Remove test database and access to it? [Y/n] y*
*Reload privilege tables now? [Y/n] y*

```
mysql -u root -p

Colocar a senha que foi criado
```

## No Mysql criar Banco , Senha , e importar tabelas
```
create database nomedobanco;
create user nomedouser identified by 'Senha123!';
grant all privileges on nomedobanco.* to nomedouser identified by 'Senha123!';
flush privileges;

```

## Acessar o Banco Criado


```

USE nomedobanco;

```

## Importar a tabela que está no diretorio

```
source  banco.sql;
SHOW tables;
QUIT;

``` 

## Descobri o IP

```
ifconfig

```

## Add informações no arquivo : my.conf

```
vim /etc/mysql/my.cnf

adicionar o essa linha com o IP que você pegou 

bind-address = 172.17.0.2


```

## Configurar os arquivos config.php e database.php


