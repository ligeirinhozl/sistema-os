#!/bin/bash

php app.php my-migration-command # run migrations
service cron start # start some services
exec apache2-foreground # main execution


# Suas configurações e lógica específicas aqui

# Exemplo de execução assíncrona
(#commando-assincrono) &

# Aguarde o comando assíncrono completar (opcional)
#wait

# Inicialize o Apache
#apache2-foreground

